#include <iostream>
#include "beer.hpp"

#include <boost/asio/io_context.hpp>
#include <boost/program_options.hpp>

#include "cmake_config.h"

#define HELP "help"
#define VERSION "version"

#define NODE "mqtt.node"
#define BROKER "mqtt.broker"

#define SW "switches"
#define PWD "password"

#define ACT "active"

#include <fstream>
#include <vector>
#include <memory>

#define CONFFILE SYSCONFDIR "/riddle_beer.ini"

#define SOUND_CLICK DATADIR "/riddle_beer/click.wav"
#define SOUND_STARTUP DATADIR "/riddle_beer/startup.wav"

int main(int argc, char *argv[])
{
    using boost::asio::io_context;
    namespace po = boost::program_options;
    using std::vector;
    using std::cout;
    using std::cerr;
    using std::endl;
    using std::ifstream;
    typedef std::string str;
    typedef unsigned uns;

    po::options_description generic("Generic options");
    generic.add_options()
        (HELP ",h", "produce help message")
        (VERSION ",v", "print version string")
    ;

    po::options_description config("Configuration");
    config.add_options()
        (BROKER ",b", po::value<str>()->value_name("URI")->default_value("localhost"), "mqtt broker uri")
        (NODE ",n", po::value<str>()->value_name("NAME")->default_value("beer"), "this node")
        (SW ",s", po::value<vector<uns>>()->value_name("ARRAY")->multitoken()->default_value({5,6,7}, "[5,6,7]"), "switch gpios")
        (PWD ",p", po::value<vector<uns>>()->value_name("ARRAY")->multitoken()->default_value({0,1,2,1,0,0,2,1}, "[0,1,2,1,0,0,2,1]"), "password")
        (ACT ",a", "do not wait for activate signal")
    ;

    po::options_description cmdline_opts;
    cmdline_opts.add(generic).add(config);

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, cmdline_opts), vm);

    str progname = ((argc >= 1) ? argv[0] : "NONAME");

    if (vm.count(HELP)) {
        cout << "Usage: " << progname << " [OPTION]...\n"
                "Reads the global config file '" CONFFILE "' if it exists.\n" << cmdline_opts << endl;
        return 0;
    }

    if (vm.count(VERSION)) {
        cout << progname << " version " << PROJECT_VERSION  << endl;
        return 0;
    }

    ifstream cfg_stream(CONFFILE);
    if (cfg_stream) {
        po::store(boost::program_options::parse_config_file(cfg_stream, config), vm);
    }
    else {
        cerr << "Warning: Cannot open config file '" << CONFFILE << "', using only command line options."<< endl;
    }

    po::notify(vm);

    sf::SoundBuffer click;
    click.loadFromFile(SOUND_CLICK);

    io_context ioc;
    mqtt_node::ptr_t beer = beer::create(ioc, vm[NODE].as<str>(), vm[BROKER].as<str>(), vm.count(ACT), vm[SW].as<vector<uns>>(), vm[PWD].as<vector<uns>>(), std::move(click), SOUND_STARTUP);
    beer->start();
    ioc.run();
    return 0;
}
