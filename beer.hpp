#ifndef BEER_HPP
#define BEER_HPP

#include "mqtt_node.hpp"
#include "pigpiopp.h"
#include <vector>
#include <SFML/Audio.hpp>

class beer : public mqtt_node
{
    static const std::string signal_reset;
    static const std::string signal_activate;
    static const std::string signal_play_startup_sound;
    static const std::string topic_gameevent;
    static const std::string gameevent_solved;

    bool active;

    std::string full_topic_gameevent;
    std::vector<unsigned> switches;
    std::vector<unsigned> password;
    std::deque<int> history;

    sf::SoundBuffer click_buffer;
    sf::Sound click_sound;

    std::string startup_music_file;
    sf::Music startup_music;

    // The pigpiopp::client has to be the last attribute
    // to ensure it will be destroyed first.
    pigpiopp::client pi;

    void edge_callback(unsigned gpio);

    /**
     * @brief Check password.
     * @return true if the entered password is correct
     */
    bool check_password();

    beer(boost::asio::io_context &ioc,
         std::string node_name,
         std::string broker_uri,
         bool active,
         std::vector<unsigned> switches,
         std::vector<unsigned> password,
         sf::SoundBuffer click,
         std::string startup_music_file);

    void reset_history();

    unsigned gpio2switch(unsigned gpio);

    bool interpret_message(mqtt::const_message_ptr msg) override;

public:
    typedef std::shared_ptr<beer> ptr_t;
    typedef std::weak_ptr<beer> wptr_t;

    template<typename ... Args>
    static ptr_t create(Args&& ... args) {
        return ptr_t(new beer(std::forward<Args>(args)...));
    }

    void start() override;
};

#endif // BEER_HPP
