#include "beer.hpp"
#include <iostream>
#include <stdexcept>
#include <boost/asio/post.hpp>
#include <boost/bind/bind.hpp>
#include <boost/bind/placeholders.hpp>

typedef mqtt_node super;

using std::string;
using boost::asio::io_context;
using boost::asio::post;
using std::cout;
using std::endl;
using std::vector;
using std::move;
using std::runtime_error;
using std::uint32_t;
using std::size_t;
using namespace boost::placeholders;
using boost::asio::post;
using std::shared_ptr;
using std::weak_ptr;
using sf::SoundBuffer;

const string beer::signal_play_startup_sound("PLAY_STARTUP_SOUND");
const string beer::signal_reset("RESET");
const string beer::signal_activate("ACTIVATE");
const string beer::topic_gameevent("gameevent");
const string beer::gameevent_solved("SOLVED");

template<class T>
static void print_vect(const T &v) {
    size_t s = v.size();
    cout << "[";
    for (size_t i = 0; i < s; i++) {
        if (i) {
            cout << ", ";
        }
        cout << v[i];
    }
    cout << "]";
}

beer::beer(io_context &ioc, string node_name, string broker_uri,
           bool active, vector<unsigned> switches, vector<unsigned> password,
           SoundBuffer click, string startup_music_file)
    : super(ioc, node_name, broker_uri),
      active(active),
      full_topic_gameevent(full_topic(topic_gameevent)),
      switches(move(switches)),
      password(move(password)),
      history(this->password.size(), -1),
      click_buffer(move(click)),
      click_sound(),
      startup_music_file(move(startup_music_file)),
      startup_music(),
      pi(nullptr, nullptr)
{
    click_sound.setBuffer(click_buffer);
    const size_t nsw = this->switches.size();
    for (unsigned d : this->password) {
        if (d >= nsw) {
            throw runtime_error("password digit out of range");
        }
    }
}

unsigned beer::gpio2switch(unsigned gpio) {
    for (unsigned i = 0; i < switches.size(); i++) {
        if (switches[i] == gpio) return i;
    }
    throw runtime_error("gpio out of range");
}

void beer::reset_history() {
    size_t s = history.size();
    for (size_t i = 0; i < s; i++) {
        history[i] = -1;
    }
}

bool beer::check_password() {
    size_t s = password.size();
    bool correct = true;
    for (size_t i = 0; i < s; i++) {
        if (history[i] != int(password[i])) {
            correct = false;
            break;
        }
    }
    return correct;
}

void beer::edge_callback(unsigned gpio) {
    if (active) {
        click_sound.play();

        unsigned sw = gpio2switch(gpio);
        history.push_back(sw);
        history.pop_front();

        cout << debug_prefix() << "Switch " << sw << " pressed" << endl;
        cout << debug_prefix() << "History: ";
        print_vect(history);
        cout << endl;

        if (check_password()) {
            cout << debug_prefix() << "Game won, sending game event SOLVED" << endl;
            startup_music.stop();
            try {
                mqtt_client.publish(full_topic_gameevent, gameevent_solved);
            }
            catch (const mqtt::exception &e) {
                cout << debug_prefix() << "Cannot send SOLVED: " << e.what() << endl;
            }
            reset_history();
            active = false;
        }
    }
}

bool beer::interpret_message(mqtt::const_message_ptr msg) {
    if (super::interpret_message(msg)) {
        return true;
    }
    const string &topic = msg->get_topic();
    string payload = msg->to_string();
    if (topic == full_topic_signal) {
        if (payload == signal_reset) {
            if (active) {
                cout << debug_prefix() << "Reset" << endl;
                startup_music.stop();
                reset_history();
                active = false;
            }
            else {
                cout << debug_prefix() << "Already reset" << endl;
            }
            return true;
        }
        else if (payload == signal_activate) {
            if (!active) {
                cout << debug_prefix() << "Activated, playing startup sound" << endl;
                startup_music.play();
                active = true;
            }
            else {
                cout << debug_prefix() << "Already active" << endl;
            }
            return true;
        }
        else if (payload == signal_play_startup_sound) {
            // Play startup sound only if active, otherwise ignore
            if (active) {
                startup_music.play();
            }
            return true;
        }
    }
    return false;
}

void beer::start() {
    startup_music.openFromFile(startup_music_file);
    cout << debug_prefix() << "Switches: ";
    print_vect(switches);
    cout << endl << debug_prefix() << "Password: ";
    print_vect(password);
    cout << endl;
    for (unsigned sw : switches) {
        pi.set_glitch_filter(sw, 10000);
        pi.register_callback(sw, pigpiopp::FALLING_EDGE, [this](unsigned gpio, unsigned level, uint32_t tick) {
            (void)level;
            (void)tick;
            weak_ptr<mqtt_node> weak_this = weak_from_this();
            post(ioc, [weak_this, gpio] {
                shared_ptr<mqtt_node> p = weak_this.lock();
                if (p) {
                    beer *b = (beer*) p.get();
                    b->edge_callback(gpio);
                }
            });
        });
    }
    if (active) {
        cout << debug_prefix() << "Playing startup sound" << endl;
        startup_music.play();
    }
    super::start();
}
