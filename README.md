# Riddle Beer
Enter the correct code on a beer dispenser.

# Install Dependencies
Add the apt repository for the `libpaho-mqtt-cpp1-dev` package (on Raspbian_11):
```
curl "https://build.opensuse.org/projects/home:elaunch/public_key" | gpg --dearmor | sudo tee /usr/share/keyrings/obs-elaunch.gpg > /dev/null
echo "deb [signed-by=/usr/share/keyrings/obs-elaunch.gpg] https://download.opensuse.org/repositories/home:/elaunch/Raspbian_11/ /" | sudo tee /etc/apt/sources.list.d/obs-elaunch.list
```
Install the necessary development packages:
```
sudo apt update
sudo apt install cmake libboost-system-dev libboost-program-options-dev libpaho-mqtt-cpp1-dev pigpio libsfml-dev
```

# Enable Pigpiod
The PiGPIO daemon controls the GPIOs:
```
sudo systemctl enable pigpiod
```

# Clone
```
git clone --recursive https://gitlab.com/nandlab/riddle_beer.git
cd riddle_beer
```
The following instructions assume you have a Debian-based Linux distribution.

# Build and Install
```
mkdir build
cd build
cmake .. -DCMAKE_BUILD_TYPE=Release -DBEER_INSTALL_SYSTEMD_SCRIPT=ON
make
sudo make install
```

# Usage
To see the help, type `riddle_beer --help`.

# Enable Autostart
```
sudo systemctl enable riddle_beer.service
```

# Configuration
The settings can be adjusted in the file `PREFIX/etc/riddle_beer.ini`.
